# Effectively visualize data across time to tell better stories
This article is intended show some tips and tricks in making clear time-based data visualizations

## Data
2018_2019_season_records.csv - Results of each game and cumulative records to date from each team's perspective  
teamcolor_dict.pickle - dictionary of each team's colours

### Article
Link: https://towardsdatascience.com/effectively-visualize-data-across-time-to-tell-better-stories-2a2c276e031e