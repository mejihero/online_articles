# NBA Physiques
This article is an example of visual data exploration.

I use historical NBA player data from Kaggle, located here:
https://www.kaggle.com/drgilermo/nba-players-stats


## Data
player_data.csv - from Kaggle dataset, includes physical / biblio data (used)
Players.csv - from Kaggle dataset, includes physical / biblio data (not used)
Seasons_Stats.csv - from Kaggle dataset, includes season stats / advanced metrics
Seasons_Stats_proc.csv - JPH processed data

### Article
Link: TBC