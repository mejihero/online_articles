# Visualising proportions
This is an article on visualising uncertainty

I use data of 18-19 NBA season, focussing on their fantasy production

## Data
- srcdata/bballref_1819_pl_box.pickle collects individual player's game box scores in dictionaries of lists
- srcdata/bballref_1819_season_tots_1000plus_mins.csv collects season totals for any players who played 1000+ minutes for the season without being traded 

### Article
Link: TBC